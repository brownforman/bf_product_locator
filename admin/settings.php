<?php

// Prevent direct access
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Define the settings page.
 *
 * @since 0.1
 */
function pl_settings_page() { ?>

	<div class="wrap">

		<?php screen_icon(); ?>

		<h2><?php esc_html_e( 'B-F Product Locator Settings', 'bf_product_locator' ) ?></h2>

		<form action="options.php" method="post">

			<?php settings_fields( 'bf-product-locator' ); ?>

			<?php do_settings_sections( 'bf-product-locator' ); ?>

			<?php submit_button(); ?>

		</form>
	</div>

<?php }

/**
 * Prints the web service type settings field.
 *
 * @since 0.1
 */
function pl_webservice_type_field() {
	$selected = bfpl_webservice_type_setting();
	?>

	<select name="_pl_webservice_type" id="_pl_webservice_type">
		<option value="proof" <?php selected( 'proof', $selected ); ?>><?php esc_html_e( 'Proof', 'bf_product_locator' ); ?></option>
		<option value="production" <?php selected( 'production', $selected ); ?>><?php esc_html_e( 'Production', 'bf_product_locator' ); ?></option>
	</select>

<?php }

/**
 * Prints the general settings section heading.
 *
 * @since 0.1
 */
function pl_settings_callback_section_general() {

	echo '<p>' . esc_html__( 'Manage B-F product location finder settings. The plugin uses B-F webservice to access records.', 'bf_product_locator' ) . '</p>';
}

/**
 * Brand Id select field.
 *
 * @since 0.1
 */
function pl_settings_callback_brand_id() {
	global $bf_product_locator;
	$options = '<option value=""></option>';
	$options .= $bf_product_locator->bf_rl->getBrandsOptions( bfpl_brand_id_setting() );
?>
	<select name="bfpl_brand_id_setting" id="bfpl_brand_id_setting">
		<?php echo $options; ?>
	</select>
<?php }

/**
 * Minor Brand Id select field.
 *
 * @since 0.1
 */
function pl_minor_brands_dropdown($brand_id, $minorbrand_id = "") {
	global $bf_product_locator;
	$options = '<option value="">Brand Id Required</option>';
	if (!empty($brand_id)){
		$options = '<option value=""></option>';
		$options .= $bf_product_locator->bf_rl->getMinorBrandsOptions( $brand_id, $minorbrand_id );
	}
	return $options;
}
function pl_settings_callback_minor_brand_id() {
	global $bf_product_locator; ?>

	<select name="bfpl_minor_brand_id_setting" id="bfpl_minor_brand_id_setting">
		<?php echo pl_minor_brands_dropdown(bfpl_brand_id_setting(), bfpl_minor_brand_id_setting() ); ?>
	</select>

<?php }

/**
 * Zoom level textbox field.
 *
 * @since 0.1
 */
function pl_settings_callback_zoom_level() { ?>

	<input name="bfpl_zoom_level_setting" type="text" id="bfpl_zoom_level_setting" value="<?php echo esc_attr( get_option( 'bfpl_zoom_level_setting', __( '15', 'bf_product_locator' ) ) ); ?>" class="small-text" />

<?php }

/**
 * Map width textbox field.
 *
 * @since 0.1
 */
function pl_settings_callback_map_width() { ?>

	<input name="bfpl_map_width_setting" type="text" id="bfpl_map_width_setting" value="<?php echo esc_attr( get_option( 'bfpl_map_width_setting', __( '680', 'bf_product_locator' ) ) ); ?>" class="small-text" />
	<span class="description"><?php esc_html_e( 'pixels', 'bf_product_locator' ); ?></span>
<?php }

/**
 * Map height textbox field.
 *
 * @since 0.1
 */
function pl_settings_callback_map_height() { ?>

	<input name="bfpl_map_height_setting" type="text" id="bfpl_map_height_setting" value="<?php echo esc_attr( get_option( 'bfpl_map_height_setting', __( '700', 'bf_product_locator' ) ) ); ?>" class="small-text" />
	<span class="description"><?php esc_html_e( 'pixels', 'bf_product_locator' ); ?></span>
<?php }
