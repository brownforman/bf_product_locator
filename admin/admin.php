<?php

// Prevent direct access
if ( ! defined( 'ABSPATH' ) ) exit;

// Call the admin setup on init.
add_action( 'init', 'pl_admin_setup' );


/**
 * Sets up the admin.
 *
 * @since 0.1
 */
function pl_admin_setup() {

	add_action( 'admin_init', 'pl_admin_register_settings' );
	add_action( 'admin_menu', 'pl_admin_menu' );
	add_filter( 'plugin_action_links', 'pl_admin_add_settings_link', 10, 2 );
	add_action( 'wp_ajax_my_action', 'my_action_callback' );
	add_action( 'admin_enqueue_scripts', 'pl_admin_enqueue_scripts' );
}

/**
 * Add the settings sections and individual settings.
 *
 * @since 0.1
 */
function pl_admin_register_settings() {

	add_settings_section( 'general_settings', __( 'General Options', 'bf_product_locator' ), 'pl_settings_callback_section_general', 'bf-product-locator' );


	// Webservice Type
 	add_settings_field( '_pl_webservice_type', '<label for="_pl_webservice_type">' . __( 'Web Service', 'bf_product_locator' ) . '</label>', 'pl_webservice_type_field', 'bf-product-locator', 'general_settings' );
 	register_setting  ( 'bf-product-locator', '_pl_webservice_type', 'esc_attr' );

 	//Brand Id
	add_settings_field( 'bfpl_brand_id_setting', '<label for="bfpl_brand_id_setting">' . __( 'Brand Id', 'bf_product_locator' ) . '</label>', 'pl_settings_callback_brand_id', 'bf-product-locator', 'general_settings' );
	register_setting  ( 'bf-product-locator', 'bfpl_brand_id_setting', 'esc_attr' );

 	//Minor Brand Id
	add_settings_field( 'bfpl_minor_brand_id_setting', '<label for="bfpl_minor_brand_id_setting">' . __( 'Minor Brand Id', 'bf_product_locator' ) . '</label>', 'pl_settings_callback_minor_brand_id', 'bf-product-locator', 'general_settings' );
	register_setting  ( 'bf-product-locator', 'bfpl_minor_brand_id_setting', 'esc_attr' );

 	// Zoom level textbox
 	add_settings_field( 'bfpl_zoom_level_setting', '<label for="bfpl_zoom_level_setting">' . __( 'Map zoom level', 'bf_product_locator' ) . '</label>', 'pl_settings_callback_zoom_level', 'bf-product-locator', 'general_settings' );
 	register_setting  ( 'bf-product-locator', 'bfpl_zoom_level_setting', 'esc_attr' );

 	// Map width textbox
 	add_settings_field( 'bfpl_map_width_setting', '<label for="bfpl_map_width_setting">' . __( 'Map width', 'bf_product_locator' ) . '</label>', 'pl_settings_callback_map_width', 'bf-product-locator', 'general_settings' );
 	register_setting  ( 'bf-product-locator', 'bfpl_map_width_setting', 'esc_attr' );

 	// Map height textbox
 	add_settings_field( 'bfpl_map_height_setting', '<label for="bfpl_map_height_setting">' . __( 'Map height', 'bf_product_locator' ) . '</label>', 'pl_settings_callback_map_height', 'bf-product-locator', 'general_settings' );
 	register_setting  ( 'bf-product-locator', 'bfpl_map_height_setting', 'esc_attr' );

	do_action( '_admin_register_settings' );
}

/**
 * Add to the settings menu.
 *
 * @since 0.1
 * @access public
 */
function pl_admin_menu() {

	add_options_page ( __( 'B-F Product Locator',  'bf_product_locator' ), __( 'B-F Product Locator',  'bf_product_locator' ), 'manage_options', 'bf-product-locator', 'pl_settings_page' );
}

/**
 * Add a direct link to the settings page from the plugins page.
 *
 * @since 0.1
 * @access public
 * @return string
 */
function pl_admin_add_settings_link( $links, $file ) {

	global $bf_product_locator;

	if ( $bf_product_locator->basename == $file ) {
		$settings_link = '<a href="' . add_query_arg( 'page', 'bf-product-locator', admin_url( 'options-general.php' ) ) . '">' . __( 'Settings', 'bf_product_locator' ) . '</a>';
		array_unshift( $links, $settings_link );
	}

	return $links;
}

/**
 * Enqueue the scripts.
 *
 * @since 0.1
 */
function pl_admin_enqueue_scripts( $page ) {
	// Add custom css or javascript
	//CSS
	//JS
	//wp_enqueue_script( 'bfpl-script', plugin_dir_path(  ) );
	wp_enqueue_script( 'bfpl-script', plugin_dir_url(__FILE__).'../js/pl_script.js', array(), "", true);

}

function my_action_callback() {
	global $bf_product_locator;
	echo pl_minor_brands_dropdown($_POST['brand_id']);
	wp_die();
}

/**
 * Admin notice error message when BF Webservice Loader is not activated.
 *
 * @since 0.1
 */
function pl_admin_notice_loader_inactive() { ?>

    <div class="error">
        <p><?php _e( 'BF Webservice Loader is not active. Please activate in the Plugins page!', 'pl_settings_page' ); ?></p>
    </div> <?php
}