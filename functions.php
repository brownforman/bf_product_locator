<?php

// Prevent direct access
if ( ! defined( 'ABSPATH' ) ) exit;


/*
 * Shortcode Product Locator
 *
 * @since 0.1
 * @return string
 */
add_shortcode('bf-product-locator', 'bf_product_shortcode_func');

/*
 * Ajax function to send email
 *
 * @since 0.1
 * @return string
 */
add_action('wp_ajax_locate', 'fetch_bf_retail_locations_callback');
add_action('wp_ajax_nopriv_locate', 'fetch_bf_retail_locations_callback');

$global_vars = array();

function s_var_error_log( $object=null ){
    ob_start();                    // start buffer capture
    var_dump( $object );           // dump the values
    $contents = ob_get_contents(); // put the buffer into a variable
    ob_end_clean();                // end capture
    error_log( $contents );        // log contents of the result of var_dump( $object )
}

function bfpl_brand_from_config() {
	ob_start(); ?>
	<input type="hidden" value="<?php echo bfpl_brand_id_setting();?>" name="rl_brand_major_id" id="rl_brand_major_id">

	<?php

	$contents = ob_get_contents();
	ob_end_clean();
	return $contents;
}

function bfpl_brand_from_user( $params = "" ) {
	global $bf_product_locator, $global_vars;

	//Get Brand List
	if (empty($params)) {
		////Full List
		$brands = $bf_product_locator->bf_rl->getBrandsOptions( bfpl_brand_id_setting() );
	} elseif (array_key_exists("brand_list", $params) && !empty($params["brand_list"]) ) {
		////List by Array
		$brands = $bf_product_locator->bf_rl->getBrandsOptionsLimited($params);
	}

	ob_start(); ?>
    <div class="row breakline">
    	<div class="column-1-1">
    		<p><?php echo $global_vars['label_brand']; ?></p>
        	<div class="pl-select">
	        	<select name="rl_brand_major_id" id="rl_brand_major_id" autofocus >
	        		<?php echo $brands; ?>
	        	</select>
        	</div>
    	</div>
    </div>
	<?php

	$contents = ob_get_contents();
	ob_end_clean();
	return $contents;
}

function bfpl_brand_id_row() {
	//Uncomment next line to set brand to the one chosen in config
	//return bfpl_brand_from_config();
	//End auto set

	//Uncomment next line to allow user to select brand from a drop down
	$params = array();
	////Pass array with key "brand_list" and an array of brand id to limit drop down options
	//$params = array("brand_list"=>array("1", "2", "6", "9", "46", "62", "63"));
	////Pass array as second argument to use
	return bfpl_brand_from_user( $params );
	//End user set
}

function bfpl_distance_row() {
	global $bf_product_locator, $global_vars;

	ob_start(); ?>

	<div class="row breakline">
        <div class="column-1-1">
        	<p><?php echo $global_vars['label_distance']; ?></p>
        	<div class="pl-select">
        	<select name="rl_distance" id="rl_distance" autofocus >
				<option value="5">5 <?php echo $global_vars['distance_system']; ?></option>
				<option selected="selected" value="10">10 <?php echo $global_vars['distance_system']; ?></option>
				<option value="15">15 <?php echo $global_vars['distance_system']; ?></option>
				<option value="20">20 <?php echo $global_vars['distance_system']; ?></option>
				<option value="25">25 <?php echo $global_vars['distance_system']; ?></option>
				<option value="50">50 <?php echo $global_vars['distance_system']; ?></option>
			</select>
			</div>
        </div>
    </div>

	<?php

	$contents = ob_get_contents();
	ob_end_clean();
	return $contents;
}

function bfpl_find_by_row() {
	global $bf_product_locator, $global_vars;

	ob_start(); ?>

    <div class="row breakline">
        <div class="column-1-1">
        	<p><?php echo $global_vars['label_find']; ?></p>
        	<input id="rl_address" name="rl_address" placeholder="<?php echo $global_vars['label_address'].' '.$global_vars['label_postal']; ?>" type="text" tabindex="1" required autofocus />
        </div>
    </div>

	<?php

	$contents = ob_get_contents();
	ob_end_clean();
	return $contents;
}

function bfpl_minor_brand_id_row() {
	global $bf_product_locator, $global_vars;

	$minor_brands = $bf_product_locator->bf_rl->getMinorBrandsOptions( bfpl_brand_id_setting(), bfpl_minor_brand_id_setting() );

	ob_start(); ?>

    <div class="row breakline">
    	<div class="column-1-1">
    		<p><?php echo $global_vars['label_product']; ?></p>
    		<div class="pl-select">
	        	<select name="rl_brand_minor_id" id="rl_brand_minor_id" autofocus >
	        		<?php echo $minor_brands; ?>
	        	</select>
        	</div>
    	</div>
    </div>

	<?php

	$contents = ob_get_contents();
	ob_end_clean();
	return $contents;
}

function bf_product_shortcode_func( $atts ) {
	global $bf_product_locator, $global_vars;

	$global_vars = shortcode_atts(array(
		"label_distance" => __( 'DISTANCE', 'bf_product_locator' ),
		"label_brand" => __( 'BRAND', 'bf_product_locator' ),
		"label_product" => __( 'PRODUCT', 'bf_product_locator' ),
		"label_find" => __( 'FIND BY', 'bf_product_locator' ),
		"label_postal" => __( 'Zip code', 'bf_product_locator' ),
		"label_address" => __( 'Enter a City/State, or ', 'bf_product_locator' ),
		"label_submit" => __( 'Submit', 'bf_product_locator' ),
		"loading_message" => __( 'Loading Retail Locator markers', 'bf_product_locator' ),
		"error_empty" => __( 'Please fill in all the required fields.', 'bf_product_locator' ),
		"distance_system" => __( 'Miles', 'bf_product_locator' ),
		"source_code" => __( '0000', 'bf_product_locator' )
	), $atts);

	if ( function_exists('getCountryCode') && ('AU' == getCountryCode()) ) {
		$global_vars['distance_system'] = 'Kilometers';
		$global_vars['label_postal'] = 'postcode';
	}

	ob_start(); ?>

	<div class="bfpl-form">

		<form id="product-locator" action="" method="post" >

			<div class="grid-container">
				<div class="row breakline">
					<div class="column-1-1">
						<h3 class="all-caps">Use our product locator below to help locate a store near you.</h3>
					</div>
				</div>

				<?php echo bfpl_distance_row(); ?>
			    <?php echo bfpl_brand_id_row(); ?>
			    <?php echo bfpl_minor_brand_id_row(); ?>
			    <?php echo bfpl_find_by_row(); ?>

			</div>

			<div class="bfpl-submit">
				<input name="product-submit" type="submit" id="product-submit" value="<?php echo $global_vars['label_submit']; ?>" class="button primary">
			</div>

			<input type="hidden" name="action" value="locate"/>

		</form>

		<div id="map-holder">

			<div id="loading" style="display: none;">
				<img src="<?php echo $bf_product_locator->plugin_url; ?>assets/images/ajax-loader.gif" title="Loader" alt="Loading" /><?php echo $global_vars['loading_message']; ?>...
			</div>

			<div id="map_canvas" name="map_canvas" style="height: <?php echo bfpl_map_height_setting();?>px; width: 100%; padding-bottom: 50px; display: none;"></div>

		</div>

		<div id="location-list" class="location-list">

			<div id="locationlist" class="locationlist"></div>

		</div>

	</div>

	<?php

	$form = ob_get_contents();

	ob_end_clean();

	bfpl_client_scripts();

	return $form;
}

function bfpl_client_scripts() {
	global $global_vars;

	wp_enqueue_script( 'bfpl-google-maps',"https://maps.googleapis.com/maps/api/js?v=3&libraries=geometry&sensor=false", "", "", True );

	$au = ( function_exists('getCountryCode') && ('AU' == getCountryCode()) );
	$df = $au ? "AU" : "";

	$php_vars = array(
		"zoom_level" => bfpl_zoom_level_setting(),
		"distance_formula" => $df,
		"distance_system" => $global_vars['distance_system'],
		"ajaxurl" => admin_url('admin-ajax.php')
	);

	wp_localize_script( 'bfpl-client-script', 'php_vars', $php_vars);
	wp_enqueue_script( 'bfpl-client-script');
}

/*
 * Fetch B-F retail locations using the webservice
 *
 * @since 0.1
 * @return string
 */
function fetch_bf_retail_locations_callback() {

	$locations = array();
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$address = parse_address_google($_POST['rl_address']);
		$distance = isset($_POST['rl_distance']) ? $_POST['rl_distance'] : '5'; // Set default to 5
		$brand_major_id = isset($_POST['rl_brand_major_id']) ? $_POST['rl_brand_major_id'] : '';
		$brand_minor_id = isset($_POST['rl_brand_minor_id']) ? $_POST['rl_brand_minor_id'] : '';
		$add_ons = array('distance' => $distance, 'brand_major_id' => $brand_major_id, 'brand_minor_id' => $brand_minor_id);

		$search = array_merge( $address, $add_ons );

		$locations = get_retail_locations($search);
	}

	$status = "OK";
	if ( empty($locations) ) {
		$status = "Empty";
	}

	$ret = array(
	    "status" => $status,
	    "data" => $locations
    );

    echo json_encode($ret);

	die();

}

/*
 * Fetch B-F retail locations using the webservice
 *
 * @since 0.1
 * @return string
 */
function get_retail_locations($search) {

	global $bf_product_locator;
	$bfrl = $bf_product_locator->bf_rl;

	$params = array('sDistance' => $search['distance'],
					'sPrimaryCity' => $search['city'],
					'sState' => $search['state'],
					'sPostalCode' => $search['zip'],
					//'sSiteID' => 3 //pass as int
	);

	if(!empty($search['brand_major_id'])) {
		$params['sBrandID'] = $search['brand_major_id'];
	}

	if(!empty($search['brand_minor_id'])){
		$params['sBrandMinorID'] = $search['brand_minor_id'];
	}

	$bf_locations = $bfrl->getRetailLocations($params);

	$result_array = array();
	foreach ($bf_locations as $key => $location) {
		array_push($result_array, array(
	        "id" => $key,
	        "latitude" => $location['latitude'],
	        "longitude" => $location['longitude'],
	        "company" => $location['company'],
	        "phone" => $location['phone'],
	        "address" => $location['address'],
	        "city" => $location['city'],
	        "state" => $location['state'],
	        "postalcode" => $location['postalcode'],
	        "countryregion" => $location['countryregion'],
	        "distance" => $location['distance']
        ));
	}

    return $result_array;
}

/*
 * Parse address using Google Geocoding
 *
 * @since 0.1
 * @return array
 */
function parse_address_google($address) {
    $url = 'http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address='.urlencode($address);
    $results = json_decode(file_get_contents($url), 1);

    $parts = array(
		'address' => array('street_number','route'),
		'city' => array('locality'),
		'state' => array('administrative_area_level_1'),
		'zip' => array('postal_code'),
    );
    if ( !empty($results['results'][0]['address_components']) ) {
      	$ac = $results['results'][0]['address_components'];
      	foreach($parts as $need=>&$types) {
        	foreach($ac as &$a) {
          		if (in_array($a['types'][0],$types)) {
          			$address_out[$need] = $a['short_name'];
          		} else if (empty($address_out[$need])) {
          			$address_out[$need] = '';
          		}
        	}
      	}
    } else {
    	echo 'Empty results';
    }
    return $address_out;
}

/**
 * Retrieves brand id from db.
 *
 * @since 0.1
 * @return string
 */
function bfpl_brand_id_setting() {
	return apply_filters( 'bfpl_brand_id_setting', get_option( 'bfpl_brand_id_setting', '' ) );
}

/**
 * Retrieves minor brand id from db.
 *
 * @since 0.1
 * @return string
 */
function bfpl_minor_brand_id_setting() {
	return apply_filters( 'bfpl_minor_brand_id_setting', get_option( 'bfpl_minor_brand_id_setting', '' ) );
}

/**
 * Retrieves map zoom level from db.
 *
 * @since 0.1
 * @return string
 */
function bfpl_zoom_level_setting() {
	return apply_filters( 'bfpl_zoom_level_setting', get_option( 'bfpl_zoom_level_setting', '15' ) );
}

/**
 * Retrieves map width from db.
 *
 * @since 0.1
 * @return string
 */
function bfpl_map_width_setting() {
	return apply_filters( 'bfpl_map_width_setting', get_option( 'bfpl_map_width_setting', '680' ) );
}

/**
 * Retrieves map height from db.
 *
 * @since 0.1
 * @return string
 */
function bfpl_map_height_setting() {
	return apply_filters( 'bfpl_map_height_setting', get_option( 'bfpl_map_height_setting', '700' ) );
}

function bfpl_webservice_type_setting() {
	return apply_filters( '_pl_webservice_type', get_option( '_pl_webservice_type', 'proof' ) );
}