<?php

/**
 * Plugin Name: B-F Product Locator
 * Description: Product Locator that uses B-F webservice to look up product details. Usage: <code>[bf-product-locator]</code>
 * Author:      Arthur/Norberto
 * Author URI:
 * Version:     0.1
 * Text Domain: bf_product_locator
 *
 * @package   B-F Product Locator
 * @version   0.1
 * @author    Arthur/Norberto
*/

// Prevent direct access
if ( ! defined( 'ABSPATH' ) ) exit;

// Global object
$bf_product_locator = new BF_Product_Locator();

/**
 * The main Template class.
 *
 * @since 0.1
 */
class BF_Product_Locator {

	/**
	 * Sets up everything.
	 *
	 * @since 0.1
	 * @access public
	 */
	public function __construct() {
		$this->setup_globals();
		$this->includes();
		$this->setup_object_vars();
		$this->setup_actions();
	}

	/**
	 * Sets up the global vars.
	 *
	 * @since 0.1
	 * @access private
	 */
	private function setup_globals() {
		// Directories and URLs
		$this->file       = __FILE__;
		$this->basename   = plugin_basename( $this->file );
		$this->plugin_dir = plugin_dir_path( $this->file );
		$this->plugin_url = plugin_dir_url( $this->file );
		$this->lang_dir   = trailingslashit( $this->plugin_dir . 'languages' );

		$this->admin_url = $this->plugin_url . 'admin';
		$this->admin_dir = $this->plugin_dir . 'admin';

		$this->lib_url = $this->plugin_url . 'lib';
		$this->lib_dir = $this->plugin_dir . 'lib';

		$this->bf_ws = null;
		$this->bf_rl = null;
	}

	/**
	 * Requires files for the plugin
	 *
	 * @since 0.1
	 * @access private
	 */
	private function includes() {
		// This file defines all of the common functions
		require( $this->plugin_dir . 'functions.php' );
		// If in the admin panel, set up the admin functions
		if ( is_admin() ) {
			require( $this->admin_dir . '/admin.php' );
			require( $this->admin_dir . '/settings.php' );
		}
	}

	/**
	 * Sets up the objects for the application.
	 *
	 * @since 0.1
	 * @access private
	 */
	private function setup_object_vars() {
	    if ( $this->is_bfws_loader_active() ) {
			// Setup global B-F webservice object
			$this->bf_ws = new BF\WebServices\BFWebServices(bfpl_webservice_type_setting());
			$this->bf_rl = new BF\RLServices\BFRLServices(bfpl_webservice_type_setting());
		} else {
			add_action( 'admin_notices', 'pl_admin_notice_loader_inactive' );
		}
	}

	/**
	 * Check if BF Webservice Loader plugin is active
	 *
	 * @since 0.1
	 * @access private
	 */
	private function is_bfws_loader_active() {

		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		// Check if plugin is active
	    if (is_plugin_active( '_bf_webservice_loader/_bf_webservice_loader.php' )) {
			return true;
		} elseif (class_exists('BF\WebServices\BFWebServices')) {
			return true;
		}
		return false;
	}

	/**
	 * Sets up the actions, filters and wp hooks.
	 *
	 * @since 0.1
	 * @access private
	 */
	private function setup_actions() {
		// Load the text domain for i18n
		add_action( 'init', array( $this, 'load_textdomain' ) );
		// Load the default and custom styles
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
	}

	/**
	 * Load the text domain.
	 *
	 * @since 0.1
	 * @access public
	 */
	public function load_textdomain() {
		$locale = get_locale();
		$locale = apply_filters( 'plugin_locale',  $locale, 'bf_product_locator' );
		$mofile = sprintf( 'bf_product_locator-%s.mo', $locale );

		$mofile_local  = $this->lang_dir . $mofile;
		$mofile_global = WP_LANG_DIR . '/bf_product_locator/' . $mofile;

		if ( file_exists( $mofile_local ) ) {
			return load_textdomain( 'bf_product_locator', $mofile_local );
		}
		if ( file_exists( $mofile_global ) ) {
			return load_textdomain( 'bf_product_locator', $mofile_global );
		}
		return false;
	}

	/**
	 * Custom styles and scripts.
	 *
	 * @since 0.1
	 * @access public
	 */
	public function enqueue_styles() {
		//CSS
		wp_enqueue_style( 'bfpl-styles', $this->plugin_url . 'assets/bf_product_locator.css' );

		//JS
		wp_register_script( 'bfpl-client-script', $this->plugin_url . 'js/pl_client_script.js', "", "", True );

	}

}