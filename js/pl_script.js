if (jQuery('#bfpl_brand_id_setting').length > 0 && jQuery('#bfpl_minor_brand_id_setting').length > 0) {
	jQuery('#bfpl_brand_id_setting').change(function(){
		var data = {
			'action' : 'my_action',
			'brand_id' : jQuery(this).val()
		};

		jQuery.post(ajaxurl, data, function(response){
			if (response !== "") {
				jQuery('#bfpl_minor_brand_id_setting').html(response);
			}

		});

	});

}