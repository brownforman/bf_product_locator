jQuery("#product-locator").submit(searchStores);
jQuery.noConflict();

var map = null;
var geocoder = null;
var markers = [];
var referenceMarker = null;
var infoWindow = null;
var address = null;
var addressLatLng = null;

function init() {
	if (map === null) {
		initializeMap();
	}
	searchInputAddress();
}

function initializeMap() {

	var mapOptions = {
		zoom: parseInt(php_vars.zoom_level), //JBG
		mapTypeControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	map = new google.maps.Map( document.getElementById("map_canvas"), mapOptions );
	geocoder = new google.maps.Geocoder();
	infoWindow = new google.maps.InfoWindow();
}

function searchInputAddress() {
	address = jQuery("#rl_address").val();
	geocoder.geocode( { "address": address}, function(results, status) {
		if (status === google.maps.GeocoderStatus.OK) {
			addressLatLng = results[0].geometry.location;
			map.setCenter(addressLatLng);
			if (referenceMarker) { referenceMarker.setMap(null); }
			referenceMarker = new google.maps.Marker({
				map: map,
				animation: google.maps.Animation.DROP,
				position: addressLatLng
			});
			referenceMarker.setIcon("http://maps.google.com/mapfiles/ms/icons/blue-dot.png");
			google.maps.event.addListener(referenceMarker, "click", function() {
				infoWindow.setContent(address);
				infoWindow.open(map, referenceMarker);
			});
			google.maps.event.addDomListener(window, "resize", initializeMap);
			google.maps.event.addDomListener(window, "load", initializeMap);
		} else {
			alert("Address not found: " + status);
		}
	});
}

function searchStores() {
	init();
	jQuery("#loading").show();
	jQuery("#map_canvas").show();
	var dataRL = jQuery("#product-locator").serialize();
	jQuery.ajax({
		url: php_vars.ajaxurl, //JBG
		type: "POST",
		data: dataRL,
		success: showStores,
		error: function(errorThrown) { alert(errorThrown); }
	});
	return false;
}

function showStores(data, status, xhr) {
	jQuery("#loading").hide();
	clearMarkers();
	var obj = jQuery.parseJSON(data);
	if (obj.status != "OK") {
		return;
	}
	for (var i in obj.data) {
		createMarker(obj.data[i]);
	}
}

function clearMarkers() {
	infoWindow.close();
	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(null);
	}
	markers.length = 0;
	document.getElementById("locationlist").innerHTML = "";
}

function createMarker(store) {
	var latlng = new google.maps.LatLng(
		parseFloat(store.latitude),
		parseFloat(store.longitude)
	);
	var marker = new google.maps.Marker({
		map: map,
		position: latlng
	});
	var distance = google.maps.geometry.spherical.computeDistanceBetween(addressLatLng, latlng);
	if (php_vars.distance_formula == "AU") {
		distance = distance / 1000;
	} else {
		distance = distance * 0.000621371192;
	}


	var html = "<b>" + store.company + "</b><br/>" + store.address + "<br/>" + store.city + ", " + store.state + " " + store.postalcode + "<br/>" + (Math.round(distance * 100) / 100) + " "+php_vars.distance_system+"<br/><a target=\"_blank\" href=\"https://maps.google.com/maps?saddr=" + address + "&daddr=" + store.address + ", " + store.city + ", " + store.state + " " + store.postalcode + "\">Get Directions</a>";

	document.getElementById("locationlist").innerHTML = "<div class=\'store\'><div class=\'store-name\'>" + store.company + "</div><div class=\'store-address\'>" + store.address + "</div><div class=\'store-city\'>" + store.city + ", " + store.state + " " + store.postalcode + "</div><div class=\'store-distance\'>" + (Math.round(distance * 100) / 100) + " "+php_vars.distance_system+"</div><div class=\'store-directions\'><a target=\"_blank\" href=\"https://maps.google.com/maps?saddr=" + address + "&daddr=" + store.address + ", " + store.city + ", " + store.state + " " + store.postalcode + "\">Get Directions</a></div></div><p class=\'newline\'><br/></p>" + document.getElementById("locationlist").innerHTML;

	google.maps.event.addListener(marker, "click", function() {
		infoWindow.setContent(html);
		infoWindow.open(map, marker);
		map.panTo(this.getPosition());
	});
	markers.push(marker);
};

function sortByKey(array, key) {
	return array.sort(function(a, b) {
	    var x = a[key]; var y = b[key];
	    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	});
}