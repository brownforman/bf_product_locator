//Include Gulp
var gulp = require('gulp');
/*var exec = require('child_process').exec;

gulp.task('rsync', function() {
	exec("rsync -a --exclude=vendor ~/working_copies/bf_contact_form/bf_contact_form ~/working_copies/wptest/site/web/app/plugins");
});*/

//Define base folders
var src = 'src/';

//Include plugins
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');//not sure why use rename instead of just naming main.js to main.min.js
var stripDebug = require('gulp-strip-debug');

//Concatenate JS Files
//one return line concats vendor files
//the other doesn't
gulp.task('scripts', function() {
    //return gulp.src([src+'js/vendor/*.js',src+'js/*.js'])
    return gulp.src(src+'js/*.js')
		.pipe(concat('main.js'))
		.pipe(stripDebug())
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
		.pipe(gulp.dest('js'));
});

//https://scotch.io/tutorials/a-quick-guide-to-using-livereload-with-gulp
var sass = require('gulp-ruby-sass');//https://www.npmjs.com/package/gulp-ruby-sass
var sourcemaps = require('gulp-sourcemaps');//https://github.com/floridoo/gulp-sourcemaps
gulp.task('sass', function() {
	return sass(src+'scss/', {
			style: 'compressed',
			sourcemap: true
		})
		.pipe(rename({suffix: '.min'}))
		.pipe(sourcemaps.write('/'))
		.pipe(gulp.dest('assets'))
});

gulp.task('watch', function() {
	// Watch .js files
	//gulp.watch(src+'js/*.js', ['scripts']);
	// Watch .scss files
	//gulp.watch(src+'scss/*.scss', ['sass']);
	gulp.watch(src+'scss/**/*.scss', ['sass']);
	//gulp.watch(src+'scss/**/*.scss', ['sass', 'rsync']);
 });

//Default Task
//gulp.task('default', ['scripts', 'sass', 'watch']);
//gulp.task('default', ['scripts']);
gulp.task('default', ['sass', 'watch']);